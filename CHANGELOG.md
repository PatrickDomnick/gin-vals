## [1.2.7](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.6...1.2.7) (2024-10-24)


### Bug Fixes

* **deps:** update module github.com/helmfile/vals to v0.37.7 ([29fd1f3](https://gitlab.com/PatrickDomnick/gin-vals/commit/29fd1f32632e090450a62d502f3299821248daf3))

## [1.2.6](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.5...1.2.6) (2024-09-19)


### Bug Fixes

* **deps:** update module github.com/helmfile/vals to v0.37.5 ([1a46e67](https://gitlab.com/PatrickDomnick/gin-vals/commit/1a46e6770f84a476942abb23255415b940af6c46))

## [1.2.5](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.4...1.2.5) (2024-08-04)


### Bug Fixes

* remove armv7 ([7ed40fc](https://gitlab.com/PatrickDomnick/gin-vals/commit/7ed40fc4711e99b536ac941b42cfce7a3e6a0f58))
* switch to alpine to support arm again ([c50cd98](https://gitlab.com/PatrickDomnick/gin-vals/commit/c50cd9847d09dca8597c324bef56c501237493f1))
* update go, goreleaser and vals ([6ea807d](https://gitlab.com/PatrickDomnick/gin-vals/commit/6ea807d0a23ab17c9348770b78fca17e304c7971))

## [1.2.5](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.4...1.2.5) (2024-08-01)


### Bug Fixes

* remove armv7 ([7ed40fc](https://gitlab.com/PatrickDomnick/gin-vals/commit/7ed40fc4711e99b536ac941b42cfce7a3e6a0f58))
* switch to alpine to support arm again ([c50cd98](https://gitlab.com/PatrickDomnick/gin-vals/commit/c50cd9847d09dca8597c324bef56c501237493f1))
* update go, goreleaser and vals ([6ea807d](https://gitlab.com/PatrickDomnick/gin-vals/commit/6ea807d0a23ab17c9348770b78fca17e304c7971))

## [1.2.4](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.3...1.2.4) (2024-06-06)


### Bug Fixes

* **deps:** update module github.com/gin-gonic/gin to v1.10.0 ([07e4601](https://gitlab.com/PatrickDomnick/gin-vals/commit/07e4601cc708c594f3208e75f9c6033d07bfc489))

## [1.2.3](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.2...1.2.3) (2024-02-08)


### Bug Fixes

* **deps:** update module github.com/helmfile/vals to v0.33.1 ([48ecbee](https://gitlab.com/PatrickDomnick/gin-vals/commit/48ecbee33f5156d569b1811d5cd8ae267c814be2))

## [1.2.2](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.1...1.2.2) (2024-01-27)


### Bug Fixes

* goreleaser version ([8fc3d09](https://gitlab.com/PatrickDomnick/gin-vals/commit/8fc3d0933daf17f7bee704266568d66fac28ebac))

## [1.2.1](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.2.0...1.2.1) (2024-01-27)


### Bug Fixes

* upgrade dependencies ([ee53433](https://gitlab.com/PatrickDomnick/gin-vals/commit/ee534337b8eca28a7664b3edb0b6d2bf33860d0e))
* upgrade to node 20 ([ed8ef0a](https://gitlab.com/PatrickDomnick/gin-vals/commit/ed8ef0a56bb977f77b51600e1f0ff5cdb8cb3bfb))

# [1.2.0](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.1.5...1.2.0) (2023-11-23)


### Features

* add put request ([247a585](https://gitlab.com/PatrickDomnick/gin-vals/commit/247a585946c64c66abd7395b199d06dc658bdbb9))

## [1.1.5](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.1.4...1.1.5) (2023-10-05)


### Bug Fixes

* **deps:** update module github.com/helmfile/vals to v0.28.0 ([28d907a](https://gitlab.com/PatrickDomnick/gin-vals/commit/28d907a62e7c7ba16ee299db513a1c7042227993))

## [1.1.4](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.1.3...1.1.4) (2023-08-31)


### Bug Fixes

* **deps:** update module github.com/helmfile/vals to v0.27.1 ([7db290f](https://gitlab.com/PatrickDomnick/gin-vals/commit/7db290f6d848822b90bbb99ad7746982fa9dd7b3))

## [1.1.3](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.1.2...1.1.3) (2023-08-10)


### Bug Fixes

* **deps:** update module github.com/helmfile/vals to v0.26.2 ([a5dd498](https://gitlab.com/PatrickDomnick/gin-vals/commit/a5dd498e46dc758465fc73c5849e1d97cb8882f3))

## [1.1.2](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.1.1...1.1.2) (2023-07-27)


### Bug Fixes

* **deps:** update module github.com/helmfile/vals to v0.26.1 ([60f80a8](https://gitlab.com/PatrickDomnick/gin-vals/commit/60f80a8fde610f4629982464366dd69dece8cca8))

## [1.1.1](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.1.0...1.1.1) (2023-06-22)


### Bug Fixes

* **deps:** update module github.com/gin-gonic/gin to v1.9.1 ([33ae9c2](https://gitlab.com/PatrickDomnick/gin-vals/commit/33ae9c205445f2d3675742e032559a6397a89978))
* **deps:** update module github.com/helmfile/vals to v0.25.0 ([e72afef](https://gitlab.com/PatrickDomnick/gin-vals/commit/e72afef5f169b4ec01922ccd55922a724990995b))

# [1.1.0](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.0.3...1.1.0) (2023-02-17)


### Bug Fixes

* branches ([c6ff09b](https://gitlab.com/PatrickDomnick/gin-vals/commit/c6ff09befe7ff70aae394d4666a36613c75cba27))
* remove build binary ([5e0beaf](https://gitlab.com/PatrickDomnick/gin-vals/commit/5e0beaf883a58925b6092ddaa762dbd06336ddc9))
* use go run ([020473c](https://gitlab.com/PatrickDomnick/gin-vals/commit/020473c2058edb976978ae838726d9674bb642b2))


### Features

* add gitpod ([79eaebe](https://gitlab.com/PatrickDomnick/gin-vals/commit/79eaebe7c69e4ba6416c63abfd03ab73e19ac49c))
* add port overwrite [ci skip] ([841cf65](https://gitlab.com/PatrickDomnick/gin-vals/commit/841cf6563aa460fe2ff1225ba5d35f1b1d64c053))
* use chainguard ([efa5071](https://gitlab.com/PatrickDomnick/gin-vals/commit/efa50711ec9c79470a3c2350a15a4b304e9ffe19))

# [1.1.0](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.0.3...1.1.0) (2023-02-17)


### Bug Fixes

* branches ([c6ff09b](https://gitlab.com/PatrickDomnick/gin-vals/commit/c6ff09befe7ff70aae394d4666a36613c75cba27))
* remove build binary ([5e0beaf](https://gitlab.com/PatrickDomnick/gin-vals/commit/5e0beaf883a58925b6092ddaa762dbd06336ddc9))
* use go run ([020473c](https://gitlab.com/PatrickDomnick/gin-vals/commit/020473c2058edb976978ae838726d9674bb642b2))


### Features

* add gitpod ([79eaebe](https://gitlab.com/PatrickDomnick/gin-vals/commit/79eaebe7c69e4ba6416c63abfd03ab73e19ac49c))
* add port overwrite [ci skip] ([841cf65](https://gitlab.com/PatrickDomnick/gin-vals/commit/841cf6563aa460fe2ff1225ba5d35f1b1d64c053))
* use chainguard ([efa5071](https://gitlab.com/PatrickDomnick/gin-vals/commit/efa50711ec9c79470a3c2350a15a4b304e9ffe19))

# [1.1.0](https://gitlab.com/PatrickDomnick/gin-vals/compare/1.0.3...1.1.0) (2023-02-17)


### Bug Fixes

* branches ([c6ff09b](https://gitlab.com/PatrickDomnick/gin-vals/commit/c6ff09befe7ff70aae394d4666a36613c75cba27))
* remove build binary ([5e0beaf](https://gitlab.com/PatrickDomnick/gin-vals/commit/5e0beaf883a58925b6092ddaa762dbd06336ddc9))
* use go run ([020473c](https://gitlab.com/PatrickDomnick/gin-vals/commit/020473c2058edb976978ae838726d9674bb642b2))


### Features

* add gitpod ([79eaebe](https://gitlab.com/PatrickDomnick/gin-vals/commit/79eaebe7c69e4ba6416c63abfd03ab73e19ac49c))
* add port overwrite [ci skip] ([841cf65](https://gitlab.com/PatrickDomnick/gin-vals/commit/841cf6563aa460fe2ff1225ba5d35f1b1d64c053))
* use chainguard ([efa5071](https://gitlab.com/PatrickDomnick/gin-vals/commit/efa50711ec9c79470a3c2350a15a4b304e9ffe19))

# 1.0.0 (2023-02-17)


### Bug Fixes

* branches ([c6ff09b](https://gitlab.com/PatrickDomnick/gin-vals/commit/c6ff09befe7ff70aae394d4666a36613c75cba27))
* error handling ([efb3589](https://gitlab.com/PatrickDomnick/gin-vals/commit/efb3589d4aefa14ca9765b50e06bd7fd9ce604aa))
* install certs ([5d1d1ac](https://gitlab.com/PatrickDomnick/gin-vals/commit/5d1d1ac3e47e2c6e81011518311047771430b985))
* remove build binary ([5e0beaf](https://gitlab.com/PatrickDomnick/gin-vals/commit/5e0beaf883a58925b6092ddaa762dbd06336ddc9))
* use go run ([020473c](https://gitlab.com/PatrickDomnick/gin-vals/commit/020473c2058edb976978ae838726d9674bb642b2))


### Features

* add gitpod ([79eaebe](https://gitlab.com/PatrickDomnick/gin-vals/commit/79eaebe7c69e4ba6416c63abfd03ab73e19ac49c))
* add port overwrite [ci skip] ([841cf65](https://gitlab.com/PatrickDomnick/gin-vals/commit/841cf6563aa460fe2ff1225ba5d35f1b1d64c053))
* Inital Version ([97045a1](https://gitlab.com/PatrickDomnick/gin-vals/commit/97045a15e3b10a1abcd63341ddf4855c2311deb6))
* Post Requests ([caa9c7f](https://gitlab.com/PatrickDomnick/gin-vals/commit/caa9c7f6669a659e6ba278060ba4b2b1ca07d338))
* use chainguard ([efa5071](https://gitlab.com/PatrickDomnick/gin-vals/commit/efa50711ec9c79470a3c2350a15a4b304e9ffe19))
